# Roster Management System

Roster management system for a logistics company with over a hundred employees, as well as various warehouses across the airport.<br>
<br>
Aims:
- Roster creation
- Auto-matching with attendance system
- Real-world data provided
reward: $10,000

Frontend rely on AJAX, similar experience as Google sheet or Google Calendar.<br>
reference Roster/Schedule Management: <br>
https://www.capterra.com/employee-scheduling-software/?utf8=%E2%9C%93&review_stars=4&users=&platform%5B1%5D=8&commit=Filter+Results&sort_options=