import * as passport from 'passport';
import * as passportLocal from 'passport-local';
import {EmployeesModel} from './services/Models/Models';
import {employeeService} from './app';
import {checkPassword} from './hash';


const LocalStrategy = passportLocal.Strategy;

passport.use('local', new LocalStrategy(
  async function(username, password, done) {
    const employees = await employeeService.retrieveAll();
    const employee = employees.find((employee:EmployeesModel)=>employee.username == username);
    if(!employee){
        return done(null,false,{message:'Incorrect name/password!'});
    }
    const match = await checkPassword(password,employee.password);
    if(match){
        return done(null,employee);
    }else{
        return done(null,false,{message:'Incorrect name/password!'});
    }
  }
));

passport.serializeUser((employee:EmployeesModel, done)=>{
    done(null,employee.id)
  })
passport.deserializeUser(async (id:number, done)=>{
    const employees = await employeeService.retrieveAll();
    const employee = employees.find((employee:EmployeesModel) => employee.id == id)
    if(employee){
        //        vvvvvvvv => req.user
        done(null,employee)
    } else {
        done(new Error('User not Found'))
    }
})