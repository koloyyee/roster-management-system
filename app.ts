import * as express from 'express';
import * as expressSession from 'express-session';
import * as path from 'path';
import  * as cors from 'cors'
import bodyParser = require('body-parser');
import { EmployeeRouter } from './routers/EmployeesRouters';
import {EmployeesServices} from './services/EmployeesServices';
import {RosterRouter} from "./routers/RostersRouters";
import {RosterService} from './services/RostersServices'
import {LocationRouter} from "./routers/LocationsRouters";
import {LocationService} from "./services/LocationsServices";
import {AnnualRouter} from './routers/AnnualsRouters';
import {AnnualService} from './services/AnnualLeaveService'
import {StatutoryRouter} from './routers/StatutoryHolidayRouters';
import {StatutoryService} from './services/StatutoryHolidayService'
import {isLoggedIn, isLoggedInByLevel} from './guards';
import * as passport from 'passport';
import './passport';
import * as Knex from 'knex';
import * as dotenv from 'dotenv';
dotenv.config()
const kConfig = require('./knexfile')
const knex = Knex(kConfig[process.env.NODE_ENV || "development"])

// [REVIEW] why would export services from app? you should pass them by dependencies injection
export const employeeService = new EmployeesServices(knex);
export const rosterService = new RosterService(knex);
export const locationService = new LocationService(knex);
export const annualsService = new AnnualService(knex);
export const statutoryService = new StatutoryService(knex);


//Server side code
const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

app.use(expressSession({
  secret: 'Roster Management Project',
  resave: true,
  saveUninitialized: true
}))

app.use(passport.initialize());
app.use(passport.session());

app.get('/login',(_req,res)=>{
  res.sendFile(path.join(__dirname,'public/login.html'));
});

app.get('/logout',(req,res)=>{
  req.logout();
  res.redirect('/login');
})

app.post('/login', passport.authenticate('local',{ failureRedirect:"/login.html"}),
  (_req,res) =>{
    res.redirect('index.html')
  })

// app.use('/employees',isLoggedInByLevel(5) ,new EmployeeRouter(employeeService).router());
app.use('/employees',isLoggedIn ,new EmployeeRouter(employeeService).router());
app.use('/rosters',isLoggedIn, new RosterRouter(rosterService).router());
app.use('/locations',isLoggedIn,  new LocationRouter(locationService).router());
app.use('/AL',isLoggedIn, new AnnualRouter(annualsService).router());
app.use('/SH', isLoggedIn ,new StatutoryRouter(statutoryService).router());

app.use(express.static('public'))
app.use(isLoggedIn,express.static('protected'));

app.use(function(_req,res,_next){
  res.end("It looks like you are accessing a wrong page.")
})

const PORT = 9000;
app.listen(PORT, ()=>{console.log(`Server listening on ${PORT}`)});

