import {RosterModel} from './Models/Models';
import * as Knex from 'knex';
import * as moment from 'moment-timezone';

// const { body } = require('express-validator/check');
// const { sanitizeBody } = require('express-validator/filter');



export class RosterService{
   
    constructor( private knex:Knex){

    }
    //CRUD with database
    async create(newRoster:RosterModel){
        const name = newRoster['name'];

        // [REVIEW] Provide a format to moment when you convert from string to a moment object, e.g. moment('2019-08-10 00:22:33', 'YYYY-MM-DD HH:mm:ss')

        // * Suggested way for handling breaking moment
        // let momentStart = null;
        // try {
        //     const startDateTime = newRoster['start_date'] +" "+ newRoster['start'];
        //     momentStart = moment(startDateTime,'YYYY-MM-DD LT').format('YYYY-MM-DD HH:mm');
        // } catch (e) {
        //     throw new Error('Start time format is invalid');
        // }

        // In backend:
        //
        //   A) Input to database scenario
        //
        //   Input (User time zone) from req.body
        //
        //   => Convert from string to moment in user time zone
        //      moment('2018-09-10 10:50:12 +08:00', 'YYYY-MM-DD LT Z')
        //
        //   => Convert datetime with time zone into a UTC timestamp
        //      .unix() // e.g. 1536547800
        //
        //   B) Get date time from database scenario
        //
        //   => Convert the UTC timestamp into a datetime object with user time zone
        //
        //      moment_tz.tz(1536547800 * 1000, 'Asia/Hong_Kong').format('YYYY-MM-DD LT')


        const startDateTime = moment(newRoster['start_date'] +" "+ newRoster['start']); // << + T+08:00 or user input
        const finishDateTime = moment(newRoster['finish_date'] +" "+ newRoster['finish']);
        const location= newRoster['location_id']
        const momentStart= moment(startDateTime,'YYYY-MM-DD LT').format('YYYY-MM-DD HH:mm')
        const momentFinish= moment(finishDateTime,'YYYY-MM-DD LT').format('YYYY-MM-DD HH:mm')

        // [REVIEW] why not accept Employee ID directly?
        const employeeId = await this.knex.select('employee.id')
        .from('employee')
        .where('employee.name','like', `%${name}%`).returning('id');
        //                                         ^^^^^^^^^^^^^^^^ [REVIEW] not necessary for SELECT. only applies for INSERT

        // [REVIEW] any location without roster will break
        const locationId = await this.knex.select('location.id')
        .from('roster')
        // vvv leftJoin
        .innerJoin('location', 'roster.location_id', 'location.id')
        .where('location_id', location).returning('id');

        
        let startTime = moment.tz(momentStart,'Asia/Hong_Kong').format("YYYY-MM-DD HH:ss");
        let finishTime = moment.tz(momentFinish,'Asia/Hong_Kong').format("YYYY-MM-DD HH:ss");

        if(employeeId !=0){
            newRoster.employee_id= employeeId[0]['id']
        } else {
           throw new Error("invalid employee")
        }
        if(locationId !=0){
             newRoster.location_id= locationId[0]['id']
        } else {
            throw new Error ("invalid location")
        }
        
        // [REVIEW] as long as you dun put the whole newRoster object into knex INSERT, you dun need delete
        delete newRoster['start_date']
        delete newRoster['finish_date']
        delete newRoster['name']
        
        console.log(newRoster)
        return await this.knex('roster')
        .insert({
            location_id: newRoster.location_id,
            employee_id: newRoster.employee_id,
            start: startTime,
            finish:finishTime
        })
    }

    async retrieveAll(){
        // return  await this.knex.select('employee.location_id').from('roster').join('employee', 'employee.id', 'roster.employee_id').select('roster.location_id', 'roster.employee_id');
        return await this.knex.select('*').from('roster')
    }
    async retrieveByLocation(id:number){
        return await this.knex.select('location.warehouse', 'employee.name', 'employee.staff_id', 'employee.username', 'roster.start')
        .from('roster')
        .innerJoin('employee','roster.employee_id', 'employee.id')
        .innerJoin('location', 'roster.location_id','location.id')
        .where('location.id', id)
    }

    async retrieveForRoster(){
        return await this.knex.select('roster.id as rosterId','employee.name','employee_id as employeeId','location.id as locationId','location.warehouse','roster.start','roster.finish' )
        .from('roster')
        .innerJoin('employee','roster.employee_id', 'employee.id')
        .innerJoin('location', 'roster.location_id','location.id')
    }

    async retrieveByName(name:string){
        // [REVIEW] roster is not necessary here because roster isn't used in SELECT column, JOIN clause, nor WHERE clause
        return await this.knex.select('location.warehouse', 'employee.name', 'employee.staff_id', 'employee.username')
        .from('roster')
        .innerJoin('employee', 'roster.employee_id', 'employee.id')
        .innerJoin('location', 'roster.location_id', 'location.id')
        .where(this.knex.raw('LOWER("name")=?', name.toLowerCase()));
        
    }
   

    async retrieveSingle(id:number){
        return await this.knex.select('*').from('roster').where('id',id);
    }

    async retrieveSingleRoster(id:number){
        return await this.knex.select('roster.id as rosterId','employee.name','employee_id as employeeId','location.id as locationId','location.warehouse','roster.start','roster.finish' )
        .from('roster')
        .innerJoin('employee','roster.employee_id', 'employee.id')
        .innerJoin('location', 'roster.location_id','location.id')
        .where('roster.id',id);
    }

    async update(id:number, update:RosterModel){

        console.log(id)
        console.log(update)
        const start = update['start_date'] + " " + update['start'];
        const finish = update['finish_date'] + " " + update['finish'];

        const name = update['name']
        const employeeId = await this.knex.select('employee.id')
        .from('employee')
        .where("name", 'like', `${name}%`)
        .returning('id')

        update.start =  moment.utc(start).toDate();
        update.finish =  moment.utc(finish).toDate();
        update.employee_id = employeeId[0].id
        console.log(employeeId)
        console.log(update.employee_id)

        delete update['name']
        delete update['start_date']
        delete update['finish_date']

         await this.knex('roster')
        .where('id', id)
        .update({
            location_id : update.location_id,
            employee_id: update.employee_id,
            start: update.start,
            finish: update.finish
        })

        return await this.retrieveSingle(id)
    }

    async delete(id:number){
        return await this.knex('roster').where('id',id).del()

    }
}
