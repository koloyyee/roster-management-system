import {EmployeesModel} from './Models/Models';
import * as Knex from 'knex';
import {hashPassword} from '../hash'

export class EmployeesServices{
   
    constructor( private knex:Knex){

    }
    //CRUD with database
    async create(newEmployee:EmployeesModel){
        
        const password = newEmployee['password']
        const hashed = await hashPassword(password)
        // const warehouse = await this.knex.select('location.warehouse')
        // .from('employee')
        // .innerJoin('location', 'employee.location_id','location.id')
        // .where('location_id',location)

        const location = newEmployee['location_id']

        // [REVIEW] employee table is not necessary
        const locationId = await this.knex.select('location.id','location.warehouse')
        .from('employee')
        .innerJoin('location','employee.location_id','location.id')
        .where('location_id', location)
        

        newEmployee['password'] = hashed;
        newEmployee['location_id'] = locationId[0].id;

        // [REVIEW] Retrieve all staff id from database, and then add 1 with transaction
        newEmployee['staff_id'] = locationId[0].warehouse+(Math.floor(Math.random()*90000)+10000)
        console.log(newEmployee['staff_id'])
        console.log(newEmployee)
        return  await this.knex.insert(newEmployee).into('employee');
    }

    async retrieveAll(){
       return  await this.knex.select('*').from('employee');
    }
    async retrieveAllWithLocation(){
        
        return await this.knex.select('employee.name','employee.staff_id', 'location.warehouse', 'employee.location_id', 'employee.id')
        .from('employee')
        .innerJoin('location','employee.location_id', 'location.id')
        .where('employee.is_deleted','false');
    }
    async retrieveSingle(id:number){
        return await this.knex.select('*').from('employee').where('id',id);
    }

    async retrieveName(){
        return await this.knex.select('name').from('employee').where('is_deleted', 'false');
    }

    async retrieveEmployee(key:string){

        return await this.knex.select('employee.name','employee.staff_id', 'location.warehouse', 'employee.location_id')
        .from('employee')
        .innerJoin('location','employee.location_id', 'location.id')
        .where(this.knex.raw('lower("name")'), 'like',`${key}%`)
        .andWhere('is_deleted', 'false');
    }

    async update(id:number, update:EmployeesModel){
        // [REVIEW] So dangerous to just accept the update object
        const result = await this.knex('employee').update(update).where('id',id);
        if(result ===1 ){
            return await this.retrieveSingle(id);
        } else {
            console.error("unable to update")
        }
        
    }

    //Not deleting the employee but change status to isDeleted = true.
    async delete(id:number){
        const result = await this.knex('employee').update('is_deleted', 'true').where('id',id).returning('is_deleted');
        if(result){
            const returnId = await this.retrieveSingle(id)
            const returnAll = await this.retrieveAll()
            console.log(returnAll)
            console.log(returnId )
        } else{
            console.error("unable to delete")
        }
        
    }
}
