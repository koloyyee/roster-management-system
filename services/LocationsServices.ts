import {LocationModel} from './Models/Models';
import * as Knex from 'knex';

export class LocationService{
   
    constructor( private knex:Knex){

    }
    //CRUD with database
    async create(newWarehouse:LocationModel){
        return  await this.knex.insert(newWarehouse).into('location');
    }
    async retrieveAll(){
        return  await this.knex.select('*').from('location');
    }
    async retrieveSingle(id:number){
        return await this.knex.select('*').from('location').where('id',id);
    }
    async update(id:number, update:LocationModel){
        return await this.knex.select('location').update(update).where('id',id);
    }

    async delete(id:number){
        return await this.knex('location').where('id',id).del()

    }
}
