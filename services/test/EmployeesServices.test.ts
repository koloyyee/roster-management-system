import { EmployeesServices } from '../EmployeesServices'
import { knex } from './TestData';
import { EmployeesModel } from '../Models/Models';


let employeesServices: EmployeesServices;
describe('Employees Services should', () => {

    employeesServices = new EmployeesServices(knex);
    
    beforeAll(async () => {
        const newUser:EmployeesModel = {

            username: "koerewtweko",
            name:"ko Co go",
            password: '1234',
            staff_id: "A23435",
            is_admin: false,
            location_id: 2
        }
       await employeesServices.create(newUser)
    })

        it('sdfo', async ()=>{
            const newUser:EmployeesModel = {

                username: "kcccko",
                name:"ko Co ccgo",
                password: '1234',
                staff_id: "A23453468",
                is_admin: false,
                location_id: 2
            }
           await employeesServices.create(newUser)
           const result = await employeesServices.retrieveAll()
           expect(result.length).toBe(7);
        })

        it("should retrieve david_ko", async () => {
            let employee = await knex.select('*').from('employee').where('username', 'david_ko').returning('id');
            let employeeId = employee[0].id
            const result = await employeesServices.retrieveSingle(employeeId);
            expect(result[0].username).toMatch('david_ko');

        })

        it("should retrieve return  1", async () => {
            const result = await employeesServices.retrieveAll();
            expect(result).toBeDefined();

        })
        it('should update a employee', async () => {
            const result = await employeesServices.update(5,
                {
                    username: "Changed",
                    name: "Changed",
                    password: "Changed",
                    staff_id: "Changed",
                    is_admin: false,
                    location_id: 3,
                    is_deleted: false
                })

            expect(result[0].username).toMatch('Changed')
        })

        it('should delete', async ()=>{
            await knex('location').returning('id')
            await knex('employee').returning('id')
            await employeesServices.delete(5)
            const result = await employeesServices.retrieveSingle(5)
            expect(result[0]['is_deleted']).toBe(true)
        })

        afterAll(()=>{
            knex.destroy();
        })
})