import * as Knex from 'knex';

export const kConfig = require('../../knexfile');
export const knex = Knex(kConfig['testing']);

// export const restart = ( async ()=>{
//    await knex.migrate.rollback();
//    await knex.migrate.latest();
//    await knex.seed.run();
// })();