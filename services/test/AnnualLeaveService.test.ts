import {AnnualService} from '../AnnualLeaveService'
import {knex} from './TestData';
let annualService: AnnualService;

describe("Annual service", ()=>{

    beforeAll(async ()=>{
        annualService = new AnnualService(knex);

    })
    it('should create user', async () =>{
        await knex('employee').returning('id')
        const newUser = {
            employee_id: 1,
            start_date: new Date(),
            finish_date: new Date()
        }
        await annualService.create(newUser)
        const result = await annualService.retrieveAll()
        expect(result.length).toBe(1)
    })
    it("should return", async()=>{
         await knex ('employee').returning('id')
        const result = await annualService.update(1,{
            employee_id: 1,
            start_date:new Date(),
            finish_date:new Date() 
        })
        expect(result[0].employee_id).toBe(1)
        
    })

    it('should delete', async ()=>{
        const result = await annualService.delete(1);
        expect(result).toBe(1);
    })
    
    afterAll(()=>{
        knex.destroy();
    })
})