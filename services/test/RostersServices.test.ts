import {RosterService} from '../RostersServices';
import {knex} from '../test/TestData';
import { RosterModel } from '../Models/Models';


let rosterService: RosterService;

describe('Roster service', ()=>{
    rosterService = new RosterService(knex);

    beforeAll( async()=>{
        await knex.migrate.rollback();
        await knex.migrate.latest();
        await knex.seed.run();
        await knex('employee').returning('id');
        await knex('location').returning('id');
    })
    xit('should create', async ()=>{
        
        const newRoster:RosterModel = {
        location_id: 4,
        employee_id: 7,
        start: new Date('2019-03-20 10:00'),
        finish: new Date('2019-03-20 15:00')
        }
        await  rosterService.create(newRoster)
    })
    it('retrieve all ', async ()=>{
        const result = await rosterService.retrieveAll();
        expect(result.length).toBe(5);
    })
    afterAll(()=>{
        knex.destroy();
    })
})