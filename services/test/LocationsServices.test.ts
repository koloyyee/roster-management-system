import {LocationService} from '../LocationsServices';
import {knex} from '../test/TestData';

describe('Location Services should', ()=>{
    let locationService :LocationService;
    beforeAll(async ()=>{
        locationService = new LocationService(knex);
        await knex.insert({
            warehouse: "D"
        }).into('location')
    })

    it("should create new user", async ()=>{
        const result = await locationService.retrieveSingle(1);
        expect(result[0]).toEqual({
            id:1,
            warehouse: "A"
        })
    })

    it("should retrieve return 1", async ()=>{
        const result = await locationService.retrieveAll();
        expect(result.length).toBeDefined();
    })

    afterAll(()=>{
        knex.destroy();
    })
})