import {AnnualModel} from './Models/Models';
import * as Knex from 'knex';

export class AnnualService{
   
    constructor( private knex:Knex){

    }
    //CRUD with database
    async create(newApplication:AnnualModel){
        return  await this.knex.insert(newApplication).into('annual');
    }
    async retrieveAll(){
        return  await this.knex.select('*').from('annual');
    }
    async retrieveSingle(id:number){
        return await this.knex.select('*').from('annual').where('id',id);
    }
    async update(id:number, update:AnnualModel){
        const result = await this.knex('annual').update(update).where('id',id);
        if (result ===1 ){
            return await this.retrieveSingle(id)
        } else {
            console.error('unabale to update')
        }
    }

    async delete(id:number){
        return await this.knex('annual').where('id',id).del()

    }
}
