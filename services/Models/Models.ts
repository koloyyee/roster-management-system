// [REVIEW] Correct syntax of interface: trailing symbol is ';' instead of ','

export interface EmployeesModel {
    id?: number,
    username: string,
    name:string,
    password: string,
    staff_id: string,
    is_admin: boolean,
    location_id: number,
    is_deleted?: boolean

}

export interface RosterModel {
    id?: number,
    location_id: number,
    employee_id: number,
    start: Date,
    finish: Date
}
export interface LocationModel {
    id?: number,
    warehouse: string
}

export interface AnnualModel {
    id?: number,
    employee_id: number,
    start_date: Date,
    finish_date: Date
}

export interface StatutoryModel {
    id?: number,
    employee_id: number,
    start_date: Date,
    finish_date: Date
}