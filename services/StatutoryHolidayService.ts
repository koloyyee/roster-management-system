import {StatutoryModel} from './Models/Models';
import * as Knex from 'knex';

export class StatutoryService{
   
    constructor( private knex:Knex){

    }
    //CRUD with database
    async create(newApplication:StatutoryModel){
        return  await this.knex.insert(newApplication).into('statutory');
    }
    async retrieveAll(){
        return  await this.knex.select('*').from('statutory');
    }
    async retrieveSingle(id:number){
        return await this.knex.select('*').from('statutory').where('id',id);
    }
    async update(id:number, update:StatutoryModel){
        return await this.knex.select('statutory').update(update).where('id',id);
    }

    async delete(id:number){
        return await this.knex('statutory').where('id',id).del()

    }
}
