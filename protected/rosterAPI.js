class RosterAPI {
    constructor() {
        this._getAllRosterPromise = null;
    }

    getAllRosters = () => {
        if (this._getAllRosterPromise == null) {
            this._getAllRosterPromise = this.fetchAllRosterData();
        }

        return this._getAllRosterPromise;
    }

    getRoster = (rId) => {
        const res = await fetch(`/rosters/allrosters/${rId}`,{
            method: "GET",
            header: {
               "Content-Type" : "application/json"
           },
        })
        
        return await res.json()
    }

    fetchAllRosterData = async () => {
        const joinRosters = await fetch('/rosters/allrosters');
        const allRosters = await joinRosters.json();
        
        return allRosters;
    }
}


let rosters;
let allRosters;
async function fetchRosterData() {

    const res = await fetch('/rosters');
    rosters = await res.json();
    
    const joinRosters = await fetch('/rosters/allrosters');
    allRosters = await joinRosters.json();
    
    showEmployeeData()
    
}

//CRUD
async function createRoster(rosters) {

    const res = await fetch('/rosters/create', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(rosters)
    })
    if(res.status === 200){
         await res.json();
         fetchRosterData()
    } else{
        let failed = document.querySelector('#modalFailed')
        failed.innerHTML = "Invalid Input(s)"
        $('.Modal').modal('hide');
    }
}

async function getRoster(rosters) {
    const res = await fetch(`/rosters/update/${roster.id}`, {
        method: "GET",
        header: {
            "Content-Type": "application/json"
        }
    })
    await res.json();
    document.querySelector('#').append(JSON.stringify(rosters));
}

async function pasteModalData (rId){

    const res = await fetch(`/rosters/allrosters/${rId}`,{
        method: "GET",
        header: {
           "Content-Type" : "application/json"
       },

    })
    
    const data = await res.json()
    
    if (data.length > 0){
    $('#modal').on('show.bs.modal', function (e) {
        document.querySelector('#name').value = data[0].name;
        document.querySelector('#rosterId').value = rId;
        console.log(rId)
        document.querySelector('#location').value = document.querySelector('#location-select').value 
        document.querySelector('#start-date').value = (data[0].start).substring(0, data[0].start.indexOf("T"));
        document.querySelector('#start').value = (data[0].start).substr((data[0].start.indexOf("T"))+1,5); 
        document.querySelector('#finish-date').value = (data[0].finish).substring(0, data[0].finish.indexOf("T"));
        document.querySelector('#finish').value = (data[0].finish).substr((data[0].finish.indexOf("T"))+1,5);
    })
    $("#modal").modal('show');
    }
    
}


async function updateRoster(id, update) {
    const res = await fetch(`/rosters/update/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(update)
    })

    await res.json()
    fetchRosterData()
}

async function deleteRoster(id) {
    const res = await fetch(`/rosters/${id}`, {
        method: "DELETE",
    });
    await res.json();
    fetchRosterData()
}