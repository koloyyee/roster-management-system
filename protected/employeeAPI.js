let employees;
let employeesName;
let employeesLocation;

async function fetchEmployeeData(){
    
    const res = await fetch('/employees');
    employees = await res.json();

    const name = await fetch('/employees/name');
    employeesName = await name.json();

    // const all = await fetch('http://localhost:9000/employees/all');
    // allEmployeesInfo = await all.json();

    showEmployeeData()
}


//CRUD
async function createEmployee(employees) {
    const res = await fetch('/employees/create', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(employees)
    })
    if(res.status === 200){
        await res.json()
        fetchEmployeeData()
        $('#modal').modal('hide');
        $('#modal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();

});
        
   } else{
       let failed = document.querySelector('#modalFailed')
       failed.innerHTML = "Invalid Input(s)"
       $('#modal').modal('hide');
   }

}

// async function getAllEmployeeInfo(employees) {
//     const res = await fetch(`http://localhost:9000/employees/all/${employee.id}`, {
//         method: "GET",
//         headers: {
//             "Content-Type": "application/json"
//         }
//     })
//     await res.json();
// }

async function getEmployee(employees) {
    const res = await fetch(`/employees/${employee.id}`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        }
    })
    await res.json();
    document.querySelector('#').append(JSON.stringify(employees))
}
async function getAllEmployee(){
     const res = await fetch(`/employees/`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        }
    })
    await res.json();
}
async function getEmployeeBySearch(key) {
    const res = await fetch(`/employees/search/${key}`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        }
    })
    const searchResult = await res.json();
    SearchResult(searchResult)
    
}

async function updateEmployee(id,employee) {
    const res = await fetch(`/employees/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(employee)
    })
    await res.json()
}

async function deleteEmployee(id) {
    const res = await fetch(`/employees/${id}`, {
        method: "DELETE",
    });
     await res.json()
      fetchEmployeeData()

}

async function retrieveByLocationId(id){
    const res = await fetch(`/employees/locationId_${id}`,{
        method:"GET",
    });
    employeesLocation =await res.json();
    
}