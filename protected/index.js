
//generate 7 columns (mon to sun)
const rosterColumn = document.querySelector('#rosterColumn');
rosterColumn.innerHTML = '';

for (let i = 0; i < 7; i++) {
    rosterColumn.innerHTML += /*html*/
        `
        <div class="col border p-2">
            <div class="column">
                <!-- Add shift trigger -->
                <button class="btn btn-outline-secondary btn-block modalBtns" data-toggle="modal" data-target="#modal" id="modalBtn${i}">+</button>
                <!-- Add shift modal -->
                <div class="modal" id="modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">New Shift</h5><small id="modalFailed" color="red" ><small>
                                <button class="close" data-dismiss="modal">×</button>
                            </div>
                            <div class="modal-body">

                                <form id="add-shift-form" >
                                    <input type="hidden" id="rosterId" name="id">
                                    

                                    <div class="form-group">
                                        <label for="name"><h6>Employee Name <small>(letters only)</small></h6> </label>
                                        <div id="autocomplete_modal">
                                        
                                        <input type="text" placeholder="" id="name" name="name" class="form-control employeeName"required autocomplete="off">
                                        <span class="close">X</span>
                                        <div id="dialog">   </div>
                                    </div>

                                    
                                    <div class="form-group mt-2">
                                        <div class="dropdown">
                                            <label for="location"><h6>Location</h6></label>
                                            <select id="location" name="location_id" class="custom-select" required >
                                                <option value="">Open this select Location</option>
                                                <option data-warehouse="A" value= '1'>A</option>
                                                <option data-warehouse="B" value= '2'>B</option>
                                                <option data-warehouse="C" value= '3'>C</option>
                                                </select>
                                            </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        
                                        <label for="Start Date"><h6>Start Date</h6></label>
                                        <input class="border mb-2" type="date" id="start-date" name="start_date" required>
                                    
                                    
                                        <label for="start"><h6>Start</h6></label>
                                        <input type="time" id = "start" name ="start" placeholder="" class="form-control mb-2" required>
                                        

                                        <div class="form-group">
                                        <label for="End Date"><h6>End Date</h6></label>
                                         <input class="border" type="date" id="finish-date" name="finish_date" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="Finish"><h6>Finish</h6></label>
                                            <input type="time" id ="finish" name ="finish" placeholder="" class="form-control" required>
                                        </div>
                                   

                                    <div class="modal-footer">
                                            <button type="submit" class="submit btn btn-primary" id="save${i}">Save</button>
                                            <input type="reset" class="reset btn btn-secondary" value="Clear"/>
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
        <div class="mt-2 perRoster list-group droptarget" id='perRoster${i}'></div>
        `;

        
    }

    //initial data on modal
    for (let elem of document.querySelectorAll('.modalBtns')) {
    
        elem.addEventListener('click', function(){
            resetForm()
            document.querySelector('#location').value = document.querySelector('#location-select').value
            document.querySelector('#start').value = "09:00";
            document.querySelector('#finish').value = "18:00";
    })
}

//initial date on modal
for (let i = 0; i < 7; i++) {
    document.querySelector(`#modalBtn${i}`).addEventListener('click', function initialDate(){
        const weekControl = document.querySelector('#camp-week');
        let weekNumber = moment(weekControl.value).format('YYYY') + '-W' + moment(weekControl.value).format('ww')
        let day = moment(weekNumber).add(i, 'days').format('YYYY-MM-DD')
        document.querySelector('#start-date').value = day
        document.querySelector('#finish-date').value = day 
    })
}



window.onload = async function fetchEmployeesData() {
    await fetchStatutoryData();
    await fetchAnnualData();
    await fetchRosterData();
    await fetchEmployeeData();
    await fetchLocationData();
    

    //name search on left col
    const nameList = employeesName.map(function (employee) {
        return employee.name
    })

    var alreadyFilled = false;

    function initDialog() {
        clearDialog();
        for (var i = 0; i < nameList.length; i++) {
            $('.dialog').append('<div>' + nameList[i] + '</div>');
        }
    }
    function clearDialog() {
        $('.dialog').empty();
    }
    $('.autocomplete input').click(function () {
        if (!alreadyFilled) {
            $('.dialog').addClass('open');
        }

    });
    $('body').on('click', '.dialog > div', function () {
        $('.autocomplete input').val($(this).text()).focus();
        $('.autocomplete .close').addClass('visible');
        alreadyFilled = true;
    });
    $('.autocomplete .close').click(function () {
        alreadyFilled = false;
        $('.dialog').addClass('open');
        $('.autocomplete input').val('').focus();
        $(this).removeClass('visible');
    });

    function match(str) {
        str = str.toLowerCase();
        clearDialog();
        for (var i = 0; i < nameList.length; i++) {
            if (nameList[i].toLowerCase().startsWith(str)) {
                $('.dialog').append('<div>' + nameList[i] + '</div>');
            }
        }
    }
    $('.autocomplete input').on('input', function () {
        $('.dialog').addClass('open');
        alreadyFilled = false;
        match($(this).val());
    });
    $('body').click(function (e) {
        if (!$(e.target).is("input, .close")) {
            $('.dialog').removeClass('open');
        }
    });
    initDialog();
    //name search on left col END
    //modal name search
   var already_Filled = false;
        
   function init_Dialog() {
       clear_Dialog();
       for (var i = 0; i < nameList.length; i++) {
           $('#dialog').append('<div>' + nameList[i] + '</div>');
       }
   }
   function clear_Dialog() {
       $('#dialog').empty();
   }
   $('#autocomplete_modal input').click(function() {
       if (!already_Filled) {
           $('#dialog').addClass('open');
       }

   });
   $('body').on('click', '#dialog > div', function() {
       $('#autocomplete_modal input').val($(this).text()).focus();
       $('#autocomplete_modal .close').addClass('visible');
       already_Filled = true;
   });
   $('#autocomplete_modal .close').click(function() {
       already_Filled = false;
       $('#dialog').addClass('open');
       $('#autocomplete_modal input').val('').focus();
       $(this).removeClass('visible');
   });

   function matching(str) {
       str = str.toLowerCase();
       clear_Dialog();
       for (var i = 0; i < nameList.length; i++) {
           if (nameList[i].toLowerCase().startsWith(str)) {
               $('#dialog').append('<div>' + nameList[i] + '</div>');
           }
       }
   }
   $('#autocomplete_modal input').on('input', function() {
       $('#dialog').addClass('open');
       already_Filled = false;
       match($(this).val());
   });
   $('body').click(function(a) {
       if (!$(a.target).is("input, .close")) {
           $('#dialog').removeClass('open');
       }
   });
   init_Dialog();
   
//modal name search end

        
    //week picker show today
    const weekNumber = moment().format('YYYY') + '-W' + moment().format('ww')
    const weekControl = document.querySelector('#camp-week'); //week-picker
    weekControl.value = weekNumber;
    weekdaysArray()
    getLocation()
    formSubmit();
};

function getLocation(){
    let initial_Location = document.querySelector('#location-select').value
    showEmployeeData(initial_Location)
    getName(initial_Location)
}

function resetForm(){
    document.querySelector(`#add-shift-form`).reset();
}

//change of week-picker value -> change date
moment.updateLocale("en", {
    week: {
        dow: 1, // First day of week is Monday
        doy: 4  // First week of year must contain 4 January (7 + 1 - 4)
    }
});

let  weekControl = document.querySelector('#camp-week'); 
weekControl.addEventListener('change', weekdaysArray)

function weekdaysArray() {
    let days = [];
    //week-picker
    let weekNumber = moment(weekControl.value).format('YYYY') + '-W' + moment(weekControl.value).format('ww')
    for (let i = 0; i < 7; i++) {
        let day = moment(weekNumber).add(i, 'days').format('MMM-Do(ddd)')
        days.push(day)
        document.querySelector(`#day${i}`).innerHTML = days[i];
    }

    fetchRosterData()

}

//End

function showEmployeeData() {
    let weekdays = [];
    let dates = [];
    let dateResults =[];
    
    let weekNumber = moment(weekControl.value).format('YYYY') + '-W' + moment(weekControl.value).format('ww')
    let initial_Location = document.querySelector('#location-select').value
    
    document.querySelector('#location-select').addEventListener('change', (event) => {
        currentLocation = event.currentTarget.value
        showEmployeeData(currentLocation)
        getName(currentLocation)
        return currentLocation
    })
    
    
    for (let i = 0; i < 7; i++) {
        let days = moment(weekNumber).add(i, 'days').format('YYYY-MM-DD')
        weekdays.push(days)
    }

    for (let i = 0; i < 7; i++) {
        document.querySelector(`#perRoster${i}`).innerHTML=''       
        };
    for (let roster of allRosters) {
        
        let shiftStart = (roster.start).substring(0, roster.start.indexOf("T"))
        let shiftEnd = (roster.finish).substring(0, roster.finish.indexOf("T"))
        let startTime = (roster.start).substr(roster.start.indexOf("T")+1,5)
        let endTime = (roster.finish).substr(roster.finish.indexOf("T")+1,5)

        for (let i = 0; i < 7; i++){
            if (parseInt(currentLocation||initial_Location) != roster.locationId){
                break;
            }else if (weekdays[i] == shiftStart) {
                
                dateResults[i] = `
                <div class = "info-form pt-1 pb-1 text-center rounded border-info mb-2 list-group-item">  
                        <p class = 'text-center h6'><strong>${roster.name}</strong></p>
                        <p>Location: <strong>${roster.warehouse}</strong>
                        <br>Start: <strong>${shiftStart}</strong><br>
                        Time: <strong>${startTime}</strong></p>
                        <p>End: <strong>${shiftEnd}</strong><br>
                        Time: <strong>${endTime}</strong>
                        </p>
                    <button class="edit btn btn-warning" onclick="pasteModalData(${roster.rosterId})" value = "edit" id="btn-edit"> Edit </button>
                    <button class="del btn btn-danger" onclick="deleteRoster(${roster.rosterId})" type= "submit" value = "delete"> Delete</button>
                </div>
                `
                
                document.querySelector(`#perRoster${i}`).innerHTML += dateResults[i];
                break;
            }
        }
    }
  
}


let currentLocation;

//populate left side 
function getName(id) {
    const locationId = parseInt(id)
    let info1 = '';
    for (let employee of employees) {
        if (locationId === employee.location_id) {
            info1 += /*html*/
                `
               <div class="list-group-item droptarget">
                    <p class = "location ${employee.warehouse}" >
                        <p class='h6' draggable="true" id="dragtarget"> ${employee.name}</p>
                        <p> id: ${employee.staff_id} </p>
                        <p class="text-success">Location: ${employee.warehouse}</p>
                    </p>
                </div>
            
               `
        }
    }
    document.querySelector('#info1').innerHTML = info1
}
//Modal form submit/ validation

//Search function
document.querySelector('#searchBar')
.addEventListener('keyup', async function getSearchValue(){
    const key = document.querySelector('#searchBar').value.toLowerCase();
    if (key !== ""){
        await getEmployeeBySearch(key)
    }else{
        getLocation()
    }
    
})

function SearchResult(result){
    let searchName = '';
    for (let employee of result) {
        searchName +=     `
               <div class="list-group-item">
                        <p class='h6'> ${employee.name}</p>
                        <p> id: ${employee.staff_id} </p>
                        <p>Location: ${employee.warehouse}</p>
                    </p>
                </div>
               `       
    }
    document.querySelector('#info1').innerHTML = searchName

}

//form submit 
function formSubmit() {
    let shiftForm = document.querySelector(`#add-shift-form`);
    shiftForm.addEventListener('submit', async function (event) {
            event.preventDefault();
            const form = event.currentTarget;
            const formData = {};

            for (let input of form) {
                if (!['submit', 'reset'].includes(input.type)) {
                    formData[input.name] = input.value;
                }

            }
            
            if (document.querySelector('#rosterId').value !== ""){
                const id = document.querySelector('#rosterId').value
                await updateRoster(id,formData);
            }else {
                delete formData["rosterId"]
                await createRoster(formData);
            }

            $('.Modal').modal('hide');
            shiftForm.reset()
        });
};

new Sortable(info1, {
    group: 'shared',
    animation: 150 
});

for (let elem of document.querySelectorAll('.perRoster')) {
    new Sortable(elem, {
        group: 'shared',
        animation: 150,
        
    })
}

//////////////

/*拖動完成後觸發*/ 
// document.addEventListener("dragend", function(event) { 

//     $('#modal').on('show.bs.modal', function (e) {
//         if ( event.target.className == "droptarget" ) { 
//             var data = event.dataTransfer.getData("Text"); 
            
//             document.querySelector('#name').value = data;
//         } 
        
//     })
//     $("#modal").modal('show');
    
// });







function logout(){
    
}
