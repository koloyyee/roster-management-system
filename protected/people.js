
window.addEventListener('load',async()=>{
    await fetchEmployeeData()
    await fetchRosterData()
    
    const nameList = employeesName.map(function (employee) {
        return employee.name
    })

    var alreadyFilled = false;

    function initDialog() {
        clearDialog();
        for (var i = 0; i < nameList.length; i++) {
            $('.dialog').append('<div>' + nameList[i] + '</div>');
        }
    }
    function clearDialog() {
        $('.dialog').empty();
    }
    $('.autocomplete input').click(function () {
        if (!alreadyFilled) {
            $('.dialog').addClass('open');
        }

    });
    $('body').on('click', '.dialog > div', function () {
        $('.autocomplete input').val($(this).text()).focus();
        $('.autocomplete .close').addClass('visible');
        alreadyFilled = true;
    });
    $('.autocomplete .close').click(function () {
        alreadyFilled = false;
        $('.dialog').addClass('open');
        $('.autocomplete input').val('').focus();
        $(this).removeClass('visible');
    });

    function match(str) {
        str = str.toLowerCase();
        clearDialog();
        for (var i = 0; i < nameList.length; i++) {
            if (nameList[i].toLowerCase().startsWith(str)) {
                $('.dialog').append('<div>' + nameList[i] + '</div>');
            }
        }
    }
    $('.autocomplete input').on('input', function () {
        $('.dialog').addClass('open');
        alreadyFilled = false;
        match($(this).val());
    });
    $('body').click(function (e) {
        if (!$(e.target).is("input, .close")) {
            $('.dialog').removeClass('open');
        }
    });
    initDialog();
    //name search on left col END
    //modal name search
   var already_Filled = false;
        
   function init_Dialog() {
       clear_Dialog();
       for (var i = 0; i < nameList.length; i++) {
           $('#dialog').append('<div>' + nameList[i] + '</div>');
       }
   }
   function clear_Dialog() {
       $('#dialog').empty();
   }
   $('#autocomplete_modal input').click(function() {
       if (!already_Filled) {
           $('#dialog').addClass('open');
       }

   });
   $('body').on('click', '#dialog > div', function() {
       $('#autocomplete_modal input').val($(this).text()).focus();
       $('#autocomplete_modal .close').addClass('visible');
       already_Filled = true;
   });
   $('#autocomplete_modal .close').click(function() {
       already_Filled = false;
       $('#dialog').addClass('open');
       $('#autocomplete_modal input').val('').focus();
       $(this).removeClass('visible');
   });

   function matching(str) {
       str = str.toLowerCase();
       clear_Dialog();
       for (var i = 0; i < nameList.length; i++) {
           if (nameList[i].toLowerCase().startsWith(str)) {
               $('#dialog').append('<div>' + nameList[i] + '</div>');
           }
       }
   }
   $('#autocomplete_modal input').on('input', function() {
       $('#dialog').addClass('open');
       already_Filled = false;
       match($(this).val());
   });
   $('body').click(function(a) {
       if (!$(a.target).is("input, .close")) {
           $('#dialog').removeClass('open');
       }
   });
   init_Dialog();
   showEmployeeData();
    loadModal();
    register();

})

function SearchResult(result){
    let searchName = '';
    for (let employee of result) {
        searchName +=     `
        <div class="p-3 mb-2 bg-light border-bottom employee_info">
        <p class='employee_name' >Id: ${employee['staff_id']}</p>
        <p class='employee_info'  >Name: ${employee.name}</p>
        <p class='employee_info text-info' id='employee_location' >Location: ${employee.warehouse}</p>
        <p>
            <button class="del btn btn-danger" onclick="deleteEmployee(${employee.id})">Delete</button>
        </p>
    </div>
               `       
    }
    document.querySelector('#user').innerHTML = searchName

}

document.querySelector('#search')
.addEventListener('keyup', async function getSearchValue(){
    const key = document.querySelector('#search').value.toLowerCase();
    await getEmployeeBySearch(key)
})


const warehouse = document.querySelector('#location-select');
warehouse.addEventListener('change', showEmployeeData)

function showEmployeeData(){
    let location = document.querySelector('#location-select').value
    const locationId = parseInt(location);
    let result = "";

    for(let employee of employees){
        if(locationId === employee.location_id){
            result += `
                <div class="p-3 mb-2 bg-light border-bottom">
                    <p>Id: ${employee['staff_id']}</p>
                    <p>Name: ${employee.name}</p>
                    <p>Location: ${employee.warehouse}</p>
                    <p>
                        <button class="del btn btn-danger" onclick="deleteEmployee(${employee.id})">Delete</button>
                    </p>
                </div>
            `

        }
    }
    document.querySelector('#user').innerHTML = result
}

function loadModal(){
    let load = document.querySelector('#loadModal')
    load.innerHTML = /*html*/
    `
    <!-- Button trigger modal -->
    <button id="employee_btn" class="btn btn-outline-primary btn-block modalBtns" data-toggle="modal" data-target="#modal"> 
        New Employee Register
      </button>
      <!-- Modal -->
      <div class="modal" id="modal">
        <div class="modal-dialog" >
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="new_employee_register">New Employee Register</h5>
              <small id="modalFailed" color="red"><small>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" id="employee_register">
            <input type="hidden" id="employeeId" name="id">
                <form method ="POST" action="/employees/create" id="employee_register_form" >
                    <div class="form-group">
                        <label for="name"><h6>Employee Name <small>(letters only)</small></h6> </label>
                        <input type="text" placeholder="" id="employee_name" name="name" class="form-control " required autocomplete="off"/>
                </div>
                    <div class="form-group">
                            <label for="name"><h6>Login Name</h6> </label>
                            <input type="text" placeholder="Login Name" id="username" name="username" class="form-control" required autocomplete="off"/>
                </div>
                    <div class="form-group">
                            <label for="name"><h6>Password <small>(length: 5-16characters)</small></h6> </label>
                            <input type="password" placeholder="Password" id="register_password" name="password" class="form-control" required/>
                    </div> 
                    <div class="form-group mt-2">
                        <div class="dropdown">
                            <label for="location"><h6>Location</h6></label>
                            <select id="location" name="location_id" class="custom-select" required>
                                <option value="">Open this select Location</option>
                                <option data-warehouse="A" value= '1' >A</option>
                                <option data-warehouse="B" value= '2'>B</option>
                                <option data-warehouse="C" value= '3'>C</option>
                                </select>
                            </div>
                        </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                    <button onclick="submit" type="submit" class="btn btn-primary">Register</button>
              </div>   
         </form>
        </div>
    </div>
   </div>
</div>
`
    
}
async function register(){
    let newRegister = document.querySelector('#employee_register_form');
    newRegister.addEventListener('submit', async function (event){
            event.preventDefault();
            const form = event.currentTarget;
            const formData = {};
    
            for (let input of form) {
                if (!['submit', 'reset'].includes(input.type)) {
                    formData[input.name] = input.value;
                }
            }
            if (document.querySelector(`#employeeId`).value === 0){
                const rosterId = document.querySelector('#rosterId').value
                    await updateEmployee(id,formData);
                    showEmployeeData(employees);
    
    
            }else {
                delete formData["employeeId"]
                await createEmployee(formData);
                showEmployeeData(employees);
                 
            }
            
    })
}

document.querySelector('#peopleSearch')
.addEventListener('keyup', async function getSearchValue(){
    const key = document.querySelector('#peopleSearch').value.toLowerCase();
    await getEmployeeBySearch(key)
})

function PeopleSearchResult(result){
    if (document.querySelector('#peopleSearch').value !== ""){
        let searchName = '';
        for (let employee of result) {
            
            searchName +=
                    `
                <div class="list-group-item">
                            <p class='h6'> ${employee.name}</p>
                            <p> id: ${employee.staff_id} </p>
                            <p>Location: ${employee.warehouse}</p>
                        </p>
                    </div>
                `
            
        }
        document.querySelector('#user').innerHTML = searchName
    }
}



