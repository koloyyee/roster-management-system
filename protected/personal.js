
window.onload = async function (){
    await fetchAnnualData();
    await fetchStatutoryData();

}

function resetForm(){
    document.querySelector(`#annualLeaveForm`).reset();
    document.querySelector(`#sickLeaveForm`).reset();
}

document.querySelector(`#modalSLBtn`).addEventListener('click', resetForm)
document.querySelector(`#modalALBtn`).addEventListener('click', resetForm)

//form submit 
function formSubmit() {
    let shiftForm = document.querySelector(`#annualLeaveForm`);
    shiftForm.addEventListener('submit', async function (event) {
            event.preventDefault();
            const form = event.currentTarget;
            const formData = {};

            for (let input of form) {
                if (!['submit', 'reset'].includes(input.type)) {
                    formData[input.name] = input.value;
                }

                await createAL(formData);
                console.log(formData)
            
            $('.Modal').modal('hide');
            shiftForm.reset()
            }});
};

//generate 7 columns (mon to sun)
// pattern="[A-Za-z\s]+"
const rosterColumn = document.querySelector('#personalRoster');
rosterColumn.innerHTML ='';
for (let i = 1; i <= 7; i++) {
    rosterColumn.innerHTML += /*html*/
     `
        <div class="col border p-2 border border-info">
            <div class="column ">
                
                
                <div class="mt-2 personalRoster list-group " id='personalRoster${i}'></div>
            </div>
        </div>
        `;
}