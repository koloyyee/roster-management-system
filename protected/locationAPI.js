let locations;

async function fetchLocationData(){
    const res = await fetch('/locations');
    locations = await res.json();
}

//CRUD
async function createLocation(locations) {
    const res = await fetch('/locations', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(locations)
    })
    await res.json();
}


async function getLocation(locations) {
    const res = await fetch(`/locations/${locations.warehouse}`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        }

    })
    await res.json();
    document.querySelector('#').append(JSON.stringify(locations))
}

async function updateLocations(id) {
    const res = await fetch(`/locations/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(rosters) //?
    })
    await res.json()
}

async function deleteLocation(id) {
    const res = await fetch(`/locations/${id}`, {
        method: "DELETE",
    });
    await res.json();
}