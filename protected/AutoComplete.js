class AutoComplete {
    constructor(container, nameList) {
        this.container = container;
        this.nameList = nameList;
    }

    initDialog = () => {
        clearDialog();
        for (var i = 0; i < this.nameList.length; i++) {
            $('.dialog', this.container).append('<div>' + this.nameList[i] + '</div>');
        }
    }
    clearDialog = () => {
        $('.dialog', this.container).empty();
    }

    init() {
        var alreadyFilled = false;
    
        $('.autocomplete input').click(() => {
            if (!alreadyFilled) {
                $('.dialog', this.container).addClass('open');
            }
    
        });
        $(this.container).on('click', '.dialog > div', () => {
            $('.autocomplete input', this.container).val($(this).text()).focus();
            // $('.autocomplete input[type=hidden]', this.container).val(employee.id).focus();
            $('.autocomplete .close', this.container).addClass('visible');
            alreadyFilled = true;
        });
        $('.autocomplete .close', this.container).click( (event) => {
            alreadyFilled = false;
            $('.dialog', this.container).addClass('open');
            $('.autocomplete input', this.container).val('').focus();
            $(event.currentTarget).removeClass('visible');
        });
    
        $('.autocomplete input', this.container).on('input', () => {
            $('.dialog', this.container).addClass('open');
            alreadyFilled = false;
            this.match($(this).val());
        });
        $('body').click((e) => {
            if (!$(e.target).is("input, .close")) {
                $('.dialog', this.container).removeClass('open');
            }
        });
        this.initDialog();
    }

    match = (str) => {
        str = str.toLowerCase();
        this.clearDialog();
        for (var i = 0; i < this.nameList.length; i++) {
            if (this.nameList[i].toLowerCase().startsWith(str)) {
                $('.dialog', this.container).append('<div>' + this.nameList[i] + '</div>');
            }
        }
    }
}

const modalAutoComplete = new AutoComplete($('#modal .employee-name'), nameList);
modalAutoComplete.init();

const searchAutoComplete = new AutoComplete($('#searchbar .input'), nameList);
searchAutoComplete.init();