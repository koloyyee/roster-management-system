import * as Knex from "knex";

exports.up = async function (knex: Knex){
    await knex.schema.createTable('annual', (table)=>{
        table.increments(),
        table.integer('employee_id').unsigned(),
        table.foreign('employee_id').references('employee.id'),
        table.date('start_date'),
        table.date('finish_date')
    })
};

exports.down = async function (knex: Knex) {
    await knex.schema.dropTableIfExists('annual');
};
