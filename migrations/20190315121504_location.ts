import * as Knex from "knex";

exports.up = async function (knex: Knex) {
    await knex.schema.createTable('location', (table)=>{
        table.increments(),
        table.string('warehouse')
    })
};

exports.down = async function (knex: Knex){
    await knex.schema.dropTable('location');
};
