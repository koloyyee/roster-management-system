import * as Knex from 'knex';

exports.up = async function(knex:Knex) {
    await knex.schema.createTable("employee", (table)=>{
        table.increments(),
        table.string('username').unique(),
        table.string('name'),
        table.string('password'),
        table.string('staff_id').unique(),
        table.boolean('is_admin').defaultTo(false),
        table.integer('location_id').unsigned(),
        table.foreign('location_id').references('location.id'),
        table.boolean('is_deleted').defaultTo(false),
        table.timestamps(false, true)
    })
  
};

exports.down = async function(knex:Knex) {
  await knex.schema.dropTable("employee"); 
};
