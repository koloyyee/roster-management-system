import * as Knex from 'knex';




exports.up = async function(knex:Knex) {
    await knex.schema.createTable("roster", (table)=>{
        table.increments(),
        table.integer('location_id').unsigned().notNullable(),
        table.foreign('location_id').references('location.id'),
        table.integer('employee_id').unsigned().notNullable(),
        table.foreign('employee_id').references('employee.id'),
        table.timestamp('start').notNullable(), //2019-03-30 19:00
        table.timestamp('finish').notNullable(), //2019-03-31 05:00
        table.timestamps(false, true)
    })
  
};

exports.down = async function(knex:Knex) {
  await knex.schema.dropTable("roster"); 
};
