import {LocationService} from '../services/LocationsServices';
import * as express from 'express';
import {Request, Response} from 'express';



export class LocationRouter {
    
    constructor(private locationService: LocationService){
        
    }

    router() {
        const router = express.Router();
        router.get('/', this.getLocation);
        router.get('/warehouse:id', this.getLocationSingle);
        router.put('/warehouse:id', this.updateLocation);
        router.post('/', this.createLocation);
        router.delete('/warehouse:id', this.deleteLocation);
        return router;
    }

    getLocation = async (_req:Request, res:Response) => {
        try{
            res.json( await this.locationService.retrieveAll())
            
        }catch(e){
            console.error(e)
            res.writeHead(500);
            res.end()
        }
    }
    getLocationSingle = async (req:Request, res:Response) => {
        try{
            const id = req.params.lId;
           res.json(await this.locationService.retrieveSingle(id));

        }catch(e){
            console.error(e)
            res.writeHead(500);
            res.end()
        }
    }
    updateLocation =  async (req:Request, res:Response) => {
        try{
            const id = req.params.id;
            const update = req.body;
            update.id = id
            res.json(await this.locationService.update(id, update));
        }catch(e){
            console.error(e)
            res.writeHead(500);
            res.end()
        }
    }
    createLocation =  async (req:Request, res:Response) => {
        try{
            const newLocation = req.body;
            await this.locationService.create(newLocation);
            res.json({success:true});

        }catch(e){
            console.error(e)
            res.writeHead(500);
            res.end()
        }
    }
    deleteLocation = async (req:Request, res:Response) => {
        try{
            const id = req.params.id;
            await this.locationService.delete(id);
            res.json({success:true});
        }catch(e){
            console.error(e)
            res.writeHead(500);
            res.end()
        }
    }
}