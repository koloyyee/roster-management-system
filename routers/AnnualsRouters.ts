import {AnnualService} from '../services/AnnualLeaveService';
import {Router, Request, Response}  from 'express';


export class AnnualRouter{

    constructor(private annualService: AnnualService ){

    }
    
    router(){
        const router = Router();
        router.get('/', this.all)
        // [REVIEW] Correct: /employee/:id
        router.get('/employee:id', this.single)
        router.post('/request', this.create)
        router.put('/change/employee:id', this.update)
        router.delete('/delete_AL:id', this.delete)
        return router;
    }
     all= async (req:Request, res:Response)=>{
        try{
            res.json(await this.annualService.retrieveAll());
        }catch(e){
            console.error(e);
            res.writeHead(500);
            res.end()
        }
    }
     single = async (req:Request, res:Response)=>{
        try{
            const id = req.params.id
            res.json( await this.annualService.retrieveSingle(id))
        }catch(e){
            console.error(e);
            res.writeHead(500);
            res.end()
        }
    }
     create = async (req:Request, res:Response)=>{
        try{
            const newApplication = req.body
            await this.annualService.create(newApplication)
            res.json({success:true})
        }catch(e){
            console.error(e);
            res.writeHead(500);
            res.end()
        }
    }
     update= async (req:Request, res:Response)=>{
        try{
            const id = req.params.id
            const update=req.body            
            update.id = id
            await this.annualService.update(id,update)
            res.json({success:true})
        }catch(e){
            console.error(e);
            res.writeHead(500);
            res.end()
        }
    }
     delete= async (req:Request, res:Response)=>{
        try{
            const id = req.params.id;
            await this.annualService.delete(id);
            res.json({success:true});
        }catch(e){
            console.error(e);
            res.writeHead(500);
            res.end()
        }
    }
}