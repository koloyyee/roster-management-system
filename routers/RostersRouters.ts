import {RosterService} from '../services/RostersServices';
import * as express from 'express';
import {Request, Response} from 'express';
import {finishDateValidation} from '../rosterModalValidation';

export class RosterRouter{
    
    
    constructor(private rosterService: RosterService){
    }
    router() {
        const router = express.Router();
        router.get('/', this.getShiftAll)
        router.get('/name/:name', this.getShiftByName)
        router.get('/roster/:id', this.getShift)
        router.get('/allrosters', this.getRoster)
        router.get('/allrosters/:id', this.getDetailSingleRoster)
        router.get('/location/:id', this.getShiftByLocation)
        router.post('/create',finishDateValidation , this.postShift)
        router.put('/update/:id', this.putShift)
        router.delete('/:id', this.deleteShift)
        return router;
    }

    getShiftAll = async (_req:Request, res:Response) => {
        try{
        res.json(await this.rosterService.retrieveAll());
        } catch(e) {
            console.log(e);
            res.writeHead(500);
            res.end()
        }   
    }
    getShiftSingle = async (req:Request, res:Response) => {
        try{
            const name = req.params.name;
            res.json(await this.rosterService.retrieveSingle(name));
            } catch(e) {
                console.log(e);
                res.writeHead(500);
                res.end()
            } 
    }

    getShiftByName =async (req:Request, res:Response) => {
        try{
            const name = req.params.name;
            res.json(await this.rosterService.retrieveByName(name));
            } catch(e) {
                console.log(e);
                res.writeHead(500);
                res.end()
            } 
    }
    getShiftByLocation = async (req:Request, res:Response)=>{
        try{
            const id = req.params.id;
            res.json(await this.rosterService.retrieveByLocation(id));
        }catch(e){
            console.log(e);
                res.writeHead(500);
                res.end()
        }

    }
    

    getRoster = async (_req:Request, res:Response)=>{
        try{
            res.json(await this.rosterService.retrieveForRoster());
        }catch(e){
            res.status(500).json({msg:e})
        }

    }

    getDetailSingleRoster = async (req:Request, res:Response) => {
        try{
            const rId = req.params.id;
            const result = await this.rosterService.retrieveSingleRoster(rId); 
            console.log(result);
            res.json(result);

        } catch(e) {    
            res.status(400).json({msg:e})
        } 
    }

    getShift = async (req:Request, res:Response) => {
        try{
            const rId = req.params.rId;
            const result = await this.rosterService.retrieveSingle(rId); 
            console.log(result);
            res.json(result);

        } catch(e) {    
            res.status(400).json({msg:e})
        } 
    }
    postShift = async (req:Request, res:Response) => {
        
        try{
            const newShift = req.body
            await this.rosterService.create(newShift)
            res.json({success:true})
            } catch(e) {
                // res.writeHead(400);
                res.status(400).json({msg:e})
            }   
    }
    putShift = async (req:Request, res:Response) => {
        try{
            const update = req.body;
            const id = parseInt(req.params.id)
            update.id = id;
            await this.rosterService.update(id, update)
            res.json({success:true})
            } catch(e) {
                console.log(e)
                res.status(500).json({msg:e});
            }   
    }
    deleteShift = async (req:Request, res:Response) => {
        try{
            const rId = req.params.id;
            await this.rosterService.delete(rId);
            res.json({success:true})
            } catch(e) {
                console.log(e);
                res.writeHead(500);
                res.end()
            }   
    }
}