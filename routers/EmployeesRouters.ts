import {EmployeesServices} from '../services/EmployeesServices';
import * as express from 'express';
import {Request, Response} from 'express';
import {employeeRegisterValidation} from '../employeeModalValidation'




export class EmployeeRouter {

    constructor(private employeeService: EmployeesServices){

    }
    //router

    router() {
        const router = express.Router();
        router.get('/', this.getAllWithLocation)
        router.get('/all', this.getAll)
        router.get('/name', this.getName)
        router.get('/:id', this.getSingle)
        router.get('/search/:key', this.getEmployeeBySearch)
        router.post('/create',employeeRegisterValidation, this.createEmployee)
        router.put('/:id', this.put)
        router.delete('/:id', this.delete)
        return router;
    }
    //CRUD
    getAll = async (_req:Request, res:Response) =>{
        try{
            res.json(await this.employeeService.retrieveAll())
        } catch(e){
            console.log(e)
            res.writeHead(500)
            res.end();
        }
    }
    getAllWithLocation = async (_req:Request, res:Response) =>{
        try{
            res.json(await this.employeeService.retrieveAllWithLocation())
        } catch(e){
            console.log(e)
            res.writeHead(500)
            res.end();
        }
    }
    getSingle = async (req:Request, res:Response) =>{
        try{
            const id = req.params.id
            res.json(await this.employeeService.retrieveSingle(id))
        } catch(e){
            console.log(e)
            res.writeHead(500)
            res.end();
        }
    }

    getName = async (req:Request, res:Response) =>{
        try{
            res.json(await this.employeeService.retrieveName())
        } catch(e){
            console.log(e)
            res.writeHead(500)
            res.end();
        }
    }

    getEmployeeBySearch = async (req:Request, res:Response) =>{
        
        const keyword = req.params.key
        console.log(keyword)
        try{
            res.json(await this.employeeService.retrieveEmployee(keyword))
        } catch(e){
            console.log(e)
            res.writeHead(500)
            res.end();
        }
    }
    


    createEmployee = async (req:Request, res:Response) =>{
        try{
            const newEmployee = req.body
            await this.employeeService.create(newEmployee)
            res.json({success: true})
        } catch(e){
            res.status(500).json(e)
        }
    }
    put = async (req:Request, res:Response) =>{
        try{
            // [REVIEW] Suggested way
            // const update:EmployeeUpdateModel = {
            //     id: req.params.id,
            //     name: req.body.name
            // };

            const update = req.body;
            const id = req.params.id;
            update.id = id;
            await this.employeeService.update(id, update)
            res.json({success: true})
        } catch(e){
            console.log(e)
            res.writeHead(500)
            res.end();
        }
    }
    delete = async (req:Request, res:Response) =>{
        try{
            
            const id = req.params.id
            await this.employeeService.delete(id)
            res.json({success: true});
        } catch(e){
            console.log(e)
            res.writeHead(500)
            res.end();
        }
    }


}