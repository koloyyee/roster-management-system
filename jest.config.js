module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  setupFiles:['<rootDir>/services/test/TestData.ts']
};