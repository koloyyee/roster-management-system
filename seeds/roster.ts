import * as Knex from "knex";

exports.seed = async function (knex: Knex){
    // Deletes ALL existing entries
    await knex('roster').del()
    await knex("employee").del()
    await knex("location").del()

    const location = await knex.batchInsert(('location'),[
        {warehouse:"A"},{warehouse:"B"},{warehouse:"C"}
    ]).returning('id');
    const warehouseA = location[0];
    const warehouseB = location[1];
    const warehouseC = location[2];

    const employee  = await knex.batchInsert(('employee'),[
        { username:"david_ko", name:"David Ko", password:"$2b$10$B9VDnWxJt1PaOXCaSbPVl.fSijMwO7FaGFC2QkPkc4KErE0PL09Pa", staff_id:"A01293", is_admin:true, location_id:warehouseA },
            { username:"peter_chan", name:"Peter Chan", password:"$2b$10$B9VDnWxJt1PaOXCaSbPVl.fSijMwO7FaGFC2QkPkc4KErE0PL09Pa", staff_id:"A01294",is_admin:false, location_id:warehouseA},
            { username:"alyson_liu", name:"Alyson Liu", password:"$2b$10$B9VDnWxJt1PaOXCaSbPVl.fSijMwO7FaGFC2QkPkc4KErE0PL09Pa", staff_id:"C14324",is_admin:true, location_id:warehouseC},
            { username:"greg_wong", name:"Greg Wong", password:"$2b$10$B9VDnWxJt1PaOXCaSbPVl.fSijMwO7FaGFC2QkPkc4KErE0PL09Pa", staff_id:"C09731",is_admin:false, location_id:warehouseC},
            { username:"mike_tai", name:"Mike Tai", password:"$2b$10$B9VDnWxJt1PaOXCaSbPVl.fSijMwO7FaGFC2QkPkc4KErE0PL09Pa", staff_id:"B90382", is_admin:true, location_id:warehouseB }
    ]).returning('id');
    const employee1 = employee[0];
    const employee2= employee[1];
    const employee3 = employee[2];
    const employee4 = employee[3];
    const employee5 = employee[4];
    await knex.batchInsert(('roster'),[{
        location_id: warehouseA,
        employee_id:employee1,
        start: "2019-03-19",
        finish:"2019-03-22"
    },{
        location_id: warehouseB,
        employee_id:employee2,
        start: "2019-03-19",
        finish:"2019-03-22"
    },
    {
        location_id: warehouseC,
        employee_id:employee3,
        start: "2019-03-19",
        finish:"2019-03-22"
    },
    {
        location_id: warehouseC,
        employee_id:employee4,
        start: "2019-03-19",
        finish:"2019-03-22"
    },
    {
        location_id: warehouseB,
        employee_id:employee5,
        start: "2019-03-19",
        finish:"2019-03-22"
    }
]).returning('id')
};
