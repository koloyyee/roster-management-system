import {check} from 'express-validator/check';
import  * as moment from 'moment'


export const finishDateValidation = [
    check('finish_date').exists().custom((value, {req})=>{ 
        let result = false;
        result = moment(req.body.start_date).isSameOrBefore(moment(value))
        if (result){
            return Promise.resolve();
        } else{
            return Promise.reject("Invalid date");
        }
    }),
    check('finish').exists().custom((value, {req})=>{ 
        let result = false;
        result = moment(req.body.start,'HH:ss').isBefore(moment(value, 'HH:ss'));
        if (result){
            return Promise.resolve();
        } else{
            return Promise.reject("Invalid time");
        }
    }) 
]

