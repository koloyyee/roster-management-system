import {check} from'express-validator/check';

export const employeeRegisterValidation = [
    check('name').not().isEmpty(),
    check('username').isAlpha('en-GB' ||'en-US').withMessage('English letters only.'),
    check('password').isLength({min:5, max:16}).withMessage('Must be 5-16 characters long.'),
    check('location_id').not().isEmpty().withMessage('Cannot be empty.')
]